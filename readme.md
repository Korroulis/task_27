# Task 27 - Session

## Create a basic express with 4 routes

**- GET /** Check if a user session exists if not redirect to login otherwise to dashboard

**- GET /create** Create a random user session

**- GET /login** Show the text login screen

**- GET /dashboard** Show the text dashboard

- Any other input after the / should prompt the user to choose one of hte above endpoints