const express = require('express');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

app.use(cookieParser());

app.use('/public', express.static( path.join(__dirname, 'browser')));

app.use(session({
    //secret:process.env.SESSION_SECRET,
    secret:process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure:false,
        httpOnly: true,
        maxAge: 60000
    }
})); 

app.get('/',(req,res) => {
    console.log(req.session);
    if (req.session.counter==undefined) {
        //req.session.counter = 0;
        res.send('Session does not exist, redirect to /login');
        // res.redirect('/login.html');
    } else {
        req.session.counter += 1;
        res.send('Session exists, redirect to /dashboard. Counter: '+ req.session.counter);
        // res.redirect('/deashboard.html');
    }  
});

//////// Create new session

app.get('/create', (req,res)=> {
    req.session.counter = 0;
    //res.send('Random user session created..');
    res.sendFile( path.join(__dirname, 'browser','index.html'));
}); 

/////// If a session exists redirect to dashboard otherwise to login

app.get('/login', (req,res)=> {
    if (req.session.counter==undefined) {
        //req.session.counter = 0;
        res.send('Login screen');
        // res.redirect('/login.html');
    } else {
        req.session.counter += 1;
        res.send('Dashboard. Visits: '+ req.session.counter);
        // res.redirect('/deashboard.html');
    } 
}); 

////// Same as above

app.get('/dashboard', (req,res)=> {
    if (req.session.counter==undefined) {
        //req.session.counter = 0;
        res.send('Session does not exist, redirect to /login');
        // res.redirect('/login.html');
    } else {
        req.session.counter += 1;
        res.send('Dashboard. Visits: '+ req.session.counter);
        // res.redirect('/deashboard.html');
    } 
}); 

///// If none of the above endpoints are provided a message should appear

app.get('/*', (req,res)=> {
    res.send('Please enter valid endpoint name');
});


app.listen(3000, ()=> console.log('app running on...'));